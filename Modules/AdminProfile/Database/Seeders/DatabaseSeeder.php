<?php
namespace Modules\AdminProfile\Database\Seeders;

use Illuminate\Database\Console\Seeds;
use Modules\AdminProfile\Entities;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RoleSeederTableSeeder::class);
         $this->call(PermissionSeederTableSeeder::class);
         $this->call(UserSeederTableSeeder::class);
    }
}
