<?php

namespace Modules\AdminProfile\Entities;

use Modules\AdminProfile\Entities\Permission;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
//    protected $fillable = [];
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'roles_permissions');
    }
}
