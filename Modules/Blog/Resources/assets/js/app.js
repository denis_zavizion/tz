
window.Vue = require('vue');
import VueRouter from 'vue-router';
var router = new VueRouter({});
Vue.component('test-component', require('./components/TestComponent.vue').default);
const app = new Vue({
    el: '#app',
    router:router

});
