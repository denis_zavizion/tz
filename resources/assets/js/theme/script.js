jQuery(document).ready(function($) {
	$('.carousel').flickity({
        'wrapAround': true,
        'pageDots': false
    });
	if($(window).width() > 767){
        $('.main_carousel').flickity({
            'wrapAround': true,
            'pageDots': false,
            'contain': true,
            'groupCells': true,
            'cellAlign' : 'center'
        });
    }
	$('.chat_open_btn').click(function(){
	    $('.chat_block').slideToggle(400);
	    $(this).toggleClass('active');
	    if($(this).hasClass('active')){
	        $(this).find('span').text('Close chat');
        }else{
            $(this).find('span').text('See chat');
        }
    });
    $('.chat_close_btn').click(function(){
        $('.chat_block').slideUp(400);
    });
    $('.dropdown_title').click(function(){
       $(this).next('.dropdown_block').fadeToggle(200);
    });
    $('.close_menu, .close_icon').click(function(){
        $('.menu').removeClass('active');
        $('.search_form').removeClass(200);
    });
    $('.search_block').click(function(){
        $('.search_form').toggleClass('active');
        $('.search_form input').val('');
    });
    $('.menu_icon').click(function () {
        $('.menu').toggleClass('active');
        $('.search_form').toggleClass('active');
    })
});