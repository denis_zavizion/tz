<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $url_data = [
            ['title'=>'1111', 'url'=>'http://url.ru444'],
            ['title'=>'33333', 'url'=>'http://url2.ru']
        ];
        $test = 'test';

        return view('index', ['url_data'=>$url_data, 'test'=>$test]);
    }

    public function getJson()
    {
        $url_data = [
            ['title'=>'fffff', 'url'=>'http://url.ru0'],
            ['title'=>'33333', 'url'=>'http://url2.ru']
        ];
        return $url_data;
    }
}
