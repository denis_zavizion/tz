<?php

namespace App\Http\Controllers\employee;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\Employee;

class EmployeeController extends Controller
{
    public function index()
    {
        return view('employee/index');
    }

    public function addEmployee(Request $request){
        $time_at = User::find($request->user_id)->first();
        User::find($request->user_id)->update(['company_id' => null]);
        Employee::insert([
            'user_id'  => $request->user_id,
            'company_id' => $request->company_id,
            'date_employment' => date('Y-m-d H:i:s', strtotime($time_at->updated_at)),
            'date_dismissal' => date("Y-m-d H:i:s")
        ]);
    }
    public function getEmployee(Request $request)
    {
        $data = $request->all();
        $actual = User::where('users.company_id', '!=', null)
            ->join('company', 'company.company_id', '=', 'users.company_id')
            ->join('country', 'country.country_id', '=', 'company.country_id')
            ->where(function($q) use ($data){
                if(isset($data['company'])){
                    $q->where('company.company_id', $data['company']);
                }
                if(isset($data['country'])){
                    $q->where('country.country_id', $data['country']);
                }
                if(isset($data['user'])){
                    $q->where('users.id', $data['user']);
                }
            })
            ->select('country.name', 'users.email', 'users.first_name', 'users.second_name', 'company.title',
                'company.company_id', 'country.country_id', 'users.id AS user_id')
            ->get();
        $fired_employees = false;
        if(isset($data['user']) || isset($data['company'])){
            $fired_employees = User::join('employee', 'employee.user_id', '=', 'users.id')
                ->join('company', 'company.company_id', '=', 'employee.company_id')
                ->join('country', 'country.country_id', '=', 'company.country_id')
                ->where(function($q) use ($data){
                    if(isset($data['company'])){
                        $q->where('company.company_id', $data['company']);
                    }
                    if(isset($data['country'])){
                        $q->where('country.country_id', $data['country']);
                    }
                    if(isset($data['user'])){
                        $q->where('users.id', $data['user']);
                    }
                })
                ->select('country.name', 'users.email', 'users.first_name', 'users.second_name', 'company.title',
                    'employee.company_id', 'country.country_id', 'users.id AS user_id', 'date_employment', 'date_dismissal')
                ->get();
        }

        return ['employees' => $actual, 'fired_employees' => $fired_employees];
    }

}
