<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * Таблица, связанная с моделью Company.
     *
     * @var string
     */
    protected $table = 'company';

    /**
     * Определение первичного ключа company_id, по умолчанию id.
     *
     * @var string
     */
    protected $primaryKey = 'company_id';

//    не учитывать дату
    public $timestamps = true;
    protected $fillable = ['title', 'country_id'];
}
