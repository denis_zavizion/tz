<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * Таблица, связанная с моделью Company.
     *
     * @var string
     */
    protected $table = 'employee';

//    не учитывать дату
    public $timestamps = false;
    protected $fillable = ['company_id', 'user_id', 'date_employment', 'date_dismissal'];
}
