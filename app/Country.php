<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * Таблица, связанная с моделью Company.
     *
     * @var string
     */
    protected $table = 'country';

    /**
     * Определение первичного ключа company_id, по умолчанию id.
     *
     * @var string
     */
    protected $primaryKey = 'country_id';

//    не учитывать дату
    public $timestamps = false;
    protected $fillable = ['name'];
}
