<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Тестовое задание #1 */
Route::get('/employee', 'employee\EmployeeController@index');
Route::post('/getEmployee', 'employee\EmployeeController@getEmployee');
Route::post('/addEmployee', 'employee\EmployeeController@addEmployee');


/* Тестовое задание #2 */
Route::get('/calc', 'ip\CalcController@index');



Route::get('/', 'IndexController@index')->name('indexPage');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/ajax/getJson', 'IndexController@getJson')->name('getJson');
Auth::routes();
//Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
//Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');


//$modules = config("module.modules");
//$path = config("module.path");
//$base_namespace = config("module.base_namespace");
//if($modules){
//    foreach ($modules as $mod => $submodules){
//        foreach ($submodules as $key => $sub){
//            if(is_string($key)){
//                $sub = $key;
//            }
//            $relativePath = "/". $mod ."/". $sub;
//            $routesPath = $path . $relativePath . "/Routes/web.php";
//
//            if(file_exists($routesPath)){
//                Route::namespace("Modules\\$mod\\$sub\Controllers")->group($routesPath);
//            }
//        }
//    }
//}
